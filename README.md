# Build files for "Nintendo Covers" album

Index file: https://complexnumbers.gitlab.io/releases/nintendo-covers/

## License
### Nintendo Covers (c) by Victor Argonov Project

Nintendo Covers is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
